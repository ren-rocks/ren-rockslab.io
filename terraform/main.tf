terraform {
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "3.0"
    }
  }
}

variable "cloudflare_api_token" {
  type        = string
}

variable "cloudflare_zone_id" {
  type        = string
}

variable "cloudflare_account_id" {
  type        = string
}

variable "domain" {
  type        = string
}

variable "address" {
  type        = string
}

variable "gitlab_verification_txt" {
  type        = string
}

provider "cloudflare" {
  api_token = var.cloudflare_api_token
}

resource "cloudflare_record" "renrocks" {
  zone_id = var.cloudflare_zone_id
  name    = "@"
  value   = var.address
  type    = "A"
  proxied = true
}

resource "cloudflare_record" "www" {
  zone_id = var.cloudflare_zone_id
  name    = "www"
  value   = var.domain
  type    = "CNAME"
  proxied = true
}

resource "cloudflare_record" "txt" {
  zone_id = var.cloudflare_zone_id
  name    = "ren.rocks"
  value   = var.gitlab_verification_txt
  type    = "TXT"
}

resource "cloudflare_page_rule" "redirect-to-root" {
  zone_id  = var.cloudflare_zone_id
  target   = "www.${var.domain}/"
  priority = 2

  actions {
    forwarding_url {
      url = "https://${var.domain}"
      status_code = 301
    }
  }
}