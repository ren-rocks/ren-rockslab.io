# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/cloudflare/cloudflare" {
  version     = "3.0.0"
  constraints = "3.0.0"
  hashes = [
    "h1:Y/wUDBz9EQTeEJm/V4/RowGQTSF5ItJOTvuqFucuqqw=",
    "zh:0fb2291957cc00d8acb04a5cae1d0f8790011dc69fb2c74b60e336d92e34d085",
    "zh:1181d4ff7220abff3742463da4c4d3475a0058781f3a9996ae1138dd0ec52172",
    "zh:1e67007b97172ae66b6a85c156814b4888e66df76ff57909305a10cfd7ebaf0d",
    "zh:2381c35a1586538fcfca9ac0d219166b000f41640a8f28edd5cd44f9dbccb795",
    "zh:4011294fa97d2a7893bb68418d0c5c446f59c605bdbfddf18d7bfd47a90d057a",
    "zh:70e86342130964280d950212de30cae8609a503be69638fc43dadfee2b9d172f",
    "zh:97b15c62b0d50fd871f31685d4569bcab2b759a3a67cf0c5307da072ad5132a1",
    "zh:aa3e22f73329b1f2730e5ecabd7878f54faaead2f089fd300257b381e5505929",
    "zh:afdf51a4fff5c7788f7b6140cb2bfa180855341bed8a2b3434b5acbcfaafe443",
    "zh:b65a51cad80e848f754d4d4d692bb6405e21379196f6ba59e62e16090a742851",
    "zh:cdc933c00625dc48204b4a7d4fbc3cb7c56e5e2afef23d0d60c751bde592a3ba",
    "zh:d23cd0f6a0f9bb6a876c95434fe537b3399c8f72c6e9fe18099a97ef1d162f4f",
    "zh:dc7ad5e4ebc6322a907d9552d06a58e160917ba084b601a205f648117893f199",
    "zh:f6d07a7cf3860e8a9b68f62e97786a37d8b5e7b34958779d2722a3d5857d1224",
  ]
}
